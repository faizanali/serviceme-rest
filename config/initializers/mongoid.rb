module Mongoid
  module Document
    def serializable_hash(options = nil)
      filter_keys = ['password_hash', 'verification_code','confirmation_token']
      h = super(options)
      h['id'] = h.delete('_id').to_s if(h.has_key?('_id'))
      filter_keys.each { |key| h.delete(key) if(h.has_key?(key)) }
      h
    end
  end
end