Rails.application.routes.draw do
  apipie

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'


  get '/uploads/grid/:model/image/:id/:filename' => 'gridfs#image'
  namespace :api do
    namespace :v1 do

      get '/search'=> 'search#search'
      resources :users do
        collection do
          post :change_password
          post :sign_in
          get :sign_out
          get 'confirm_email/:token', to: :confirm_email
          post '/auth/:type/:provider', to: :authenticate, constraints: {type: /client|supplier/, provider: /google|facebook/}
        end
        member do
          get :resend_code
          post :verify_code
        end
      end

      resources :clients do
        member do
          post :checkout
        end
        resources :offers
      end

      resources :offers
      resources :suppliers do
        member do
          get :mark_favourite
        end
      end

      resources :bids
      resources :categories, only: :index do
        member do
          get :subcategories
          get :offers
        end
        collection do
          get :trending
          get :subcategories
        end
      end


    end
  end
end
