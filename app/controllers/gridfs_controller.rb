class GridfsController < ApplicationController

def image
  @user = User.find_by id: params[:id]
  content = @user.image.read
  if stale?(etag: content, last_modified: @user.updated_at.utc, public: true)
    send_data content, type: @user.image.file.content_type, disposition: "inline"
    expires_in 0, public: true
  end
end

end