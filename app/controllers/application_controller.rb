class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #  protect_from_forgery with: :null_session

   before_filter :set_ua



   def current_device
     @current_device ||= Device.find_by(token: request.headers["Token"])
     @current_device
   end


   def current_user
     @current_user ||= @current_device.try(:user)
     @current_user
   end

   def authenticate_user
     render json: {message: 'Unauthorize token'}, status: 401 unless current_device
   end


  private

  def set_ua
    Util.user_agent = request.env['HTTP_USER_AGENT']
  end
end
