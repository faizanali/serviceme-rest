module Api
  module V1
    class ClientsController < ApplicationController


      def index
        clients = Client.all
        render json: clients
      end

      def create
        client = Client.new client_params
        if client.save
          render json: {client: client, message: 'Client created successfully'}, status: 200
        elsif client.signing_up_with_existing_number?
          client = Client.find_by(mobile: client_params[:mobile])
          client.update(verification_code: Util.random)
          client.send_verification
          render json: {id: client.id.to_s, message: 'Phone number already exist'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: client.errors}, status: 422
        end
      end

      def show
        client = Client.find params[:id]
        render json: client
      end

      def edit
        client = Client.find params[:id]
        render json: client
      end

      def update
        client = Client.find params[:id]
        if client.update client_params
          render json: {client: client, message: 'Client updated successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: client.errors}, status: 422
        end
      end

      def destroy
        client = Client.find params[:id]
        if client.destroy
          render json: {message: 'Client deleted successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: client.errors}, status: 422
        end
      end


      def checkout
        client = Client.find params[:id]
        billing = Billing.new(client, params[:payment])
        billing.process_payment
        if billing.success?
          render json: {message: 'Checkout successful'}, status: 200
        else
          render json: {message: 'Checkout failed', errors: billing.errors}, status: 422
        end
      end

      protected

      def client_params
        params.require(:client).permit(:city,:country,:image,:first_name,:last_name, :email, :mobile,:password, :password_confirmation,:from_mobile,:step)
      end
    end
  end
end
