module Api
  module V1
    class CategoriesController < ApplicationController

      def index
        categories = Category.main
        render json: categories
      end

      def subcategories
        category = Category.find_by id: params[:id]
        render json: (category.try(:subcategories) || Category.sub)
      end

      def offers
        category = Category.find_by id: params[:id]
        render json: category.offers
      end

      def trending
        render json: Category.trending
      end

    end
  end
end
