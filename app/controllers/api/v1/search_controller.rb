module Api
  module V1
    class SearchController < ApplicationController


      def search
        categories = Category.search params[:q], limit: 5
        suppliers = Supplier.search params[:q], limit: 5
        # render json: results.map { |r| {id: r.id.to_s, name: r.try(:company_name) || r.try(:name), type: r.class.to_s.downcase }}
        render json: {suppliers: suppliers.map { |r| {id: r.id.to_s, name: r.try(:company_name) || r.try(:name)}},
                      categories: categories.map { |r| {id: r.id.to_s, name: r.try(:company_name) || r.try(:name)}}}
      end

    end
  end
end