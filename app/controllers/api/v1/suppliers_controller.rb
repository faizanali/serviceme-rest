module Api
  module V1
    class SuppliersController < ApplicationController

      before_action :authenticate_user, only: [:mark_favourite]

      def index
        suppliers = Supplier.filter(params,current_user).page(params[:page] || 1)
        render json: { suppliers: suppliers, total_pages: suppliers.total_pages}
      end

      def create
        supplier = Supplier.new supplier_params
        if supplier.save
          render json: {supplier: supplier, message: 'Supplier created successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: supplier.errors}, status: 422
        end
      end

      def show
        supplier = Supplier.find_by id: params[:id]
        if supplier
          render json: {supplier: supplier, message: 'Supplier found successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: 'Supplier not found'}, status: 422
        end
      end

      def mark_favourite
        supplier = Supplier.find params[:id]
        current_user.favourites << supplier
        current_user.save
        render json: {supplier: supplier, message: 'Supplier Marked as favourite'}, status: 200
      end

      def edit
        supplier = Supplier.find params[:id]
        render json: supplier
      end

      def update
        supplier = Supplier.find params[:id]
        if supplier.update supplier_params
          render json: {supplier: supplier, message: 'Supplier updated successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: supplier.errors}, status: 422
        end
      end

      def destroy
        supplier = Supplier.find params[:id]
        if supplier.destroy
          render json: {message: 'Supplier deleted successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: supplier.errors}, status: 422
        end
      end


      protected

      def supplier_params
        params.require(:supplier).permit(:status,:account_name,:bank_name,:account_no,:image,:swift_code,:trade_license,:company_name,
                                         :is_company,:first_name,:last_name, :email,:mobile,:password, :password_confirmation,
                                         category_ids: [],image_attributes: [:id,:file],address_attributes: [:street_address,:landmark,:city,:country,:area])
      end
    end
  end
end
