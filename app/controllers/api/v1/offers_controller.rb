module Api
  module V1
    class OffersController < ApplicationController

      before_action :set_client, if: 'params[:client_id].present?'

      def index
        offers = @client.offers
        render json: offers
      end

      def create
        offer = @client.offers.new offer_params
        if offer.save
          render json: {offer: offer, message: 'Offer created successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: offer.errors}, status: 422
        end
      end

      def show
        offer = Offer.find_by id: params[:id]
        if offer
          render json: {offer: offer, message: 'Offer found successfully'}
        else
          render json: {message: 'Error(s) occurred',errors: "Offer not found"}, status: 422
        end
      end

      def edit
        offer = Offer.find_by id: params[:id]
        render json: offer
      end

      def update
        offer = @client.offers.find_by id: params[:id]
        if offer.update offer_params
          render json: {offer: offer, message: 'Offer updated successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: offer.errors}, status: 422
        end
      end

      def destroy
        offer = @client.offers.find_by id: params[:id]
        if offer.destroy
          render json: {message: 'Offer deleted successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: offer.errors}, status: 422
        end
      end


      def bids
        offer = Offer.find_by id: params[:id]
        if offer
          render json: {bids: offer.bids}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: 'Offer not found'}, status: 422
        end
      end


      protected

      def set_client
        @client = Client.find_by(id: params[:client_id])
        render json: {message: 'Error(s) occurred', errors: "Client not found"}, status: 422 unless @client
      end

      def offer_params
        params.require(:offer).permit(:category_id,:currency,:bid_type,:location, :done_in, :date_time, :additional_req,:has_budget, :price, detail: OfferDetail.new(params[:offer][:category_id]).params)
      end


    end
  end
end
