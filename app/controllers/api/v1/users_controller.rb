module Api
  module V1
    class UsersController < ApplicationController

      before_action :authenticate_user, only: [:sign_out,:change_password]
      before_action :process_params, only: :sign_in

      before_action :set_user, only: [:verify_code, :resend_code]

      api!
      def sign_in
        user = User.find_by(email: params[:email]) unless params[:type].present?
        user = User.find_by(email: params[:email],_type: params[:type].capitalize) if params[:type].present?
        if !user && (params[:email].length == 9 || params[:email].length > 11)
          user = User.find_by(mobile: /#{params[:email]}\z/)
        end
        if user && user.authenticate(params[:password])
          render json: { user: user, token: user.sign_in }, status: 200
        else
          render json: { message: 'Invalid email/mobile or password' }, status: 401
        end
      end


      def sign_out
        current_device.destroy
        render json: {message: 'User sign out successful'}
      end

      def verify_code
        if params[:code] == @user.verification_code
          @user.activate
          render json: { user: @user, token: @user.sign_in}, status: 200
        else
          render json: { message: 'Invalid Verification Code' }, status: 401
        end
      end

      def confirm_email
        user = User.find_by(confirmation_token: params[:token])
        if user
          user.update(email_verified: true)
          render json: { message: "Email Confirmed"}
        else
          render json: { message: 'Invalid Confirmation Code' }, status: 422
        end
      end

      def resend_code
        @user.update(verification_code: Util.random)
        @user.send_verification
        if @user.errors.empty?
          render json: {message: 'Code sent'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: @user.errors}, status: 422
        end

      end

      def authenticate
        @oauth = "Oauth::#{params[:provider].titleize}".constantize.new(params)
        if @oauth.authorized?
          user = params[:type].titleize.constantize.from_auth(@oauth.formatted_user_data)
          if user
            render json: { user: user, token: user.sign_in }, status: 200
          else
            render json: {message: 'User not found'}, status: 422 unless @user
          end
        else
          render json: {message: 'User not found'}, status: 422 unless @user
        end
      end


      def change_password
        if current_user.change_password(params)
          render json: { message: 'Password Change Successful' }, status: 200
        else
          render json: { message: 'Error(s) Occurred', errors: current_user.errors}, status: 401
        end
      end

      private

      def process_params
        if (params[:email] =~ /^[0-9|\+][0-9]*$/) == 0
          if params[:email].length < 9 || params[:email].length > 13
            render json: { message: 'Invalid email/mobile or password' }, status: 401
          elsif params[:email].length == 10 || params[:email].length == 13
            params[:email].sub!(/^[0|\+]/, '')
          end
        end
      end

      def set_user
        @user = User.find params[:id]
        render json: {message: 'User not found'}, status: 422 unless @user
      end
    end
  end
end
