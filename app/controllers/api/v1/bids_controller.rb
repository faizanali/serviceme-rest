module Api
  module V1
    class BidsController < ApplicationController

      before_action :authenticate_user

      def index
        bids = Bid.all
        render json: bids
      end

      def create
        bid = current_user.bids.new bid_params
        if bid.save
          render json: {bid: bid, message: 'Bid created successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: bid.errors}, status: 422
        end
      end

      def show
        bid = current_user.bids.find params[:id]
        render json: bid
      end

      def edit
        bid = current_user.bids.find params[:id]
        render json: bid
      end

      def update
        bid = current_user.bids.find params[:id]
        if bid.update bid_params
          render json: {bid: bid, message: 'Bid updated successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: bid.errors}, status: 422
        end
      end

      def destroy
        bid = current_user.bids.find params[:id]
        if bid.destroy
          render json: {message: 'Bid deleted successfully'}, status: 200
        else
          render json: {message: 'Error(s) occurred', errors: bid.errors}, status: 422
        end
      end


      protected

      # def set_supplier
      #   current_user = Supplier.find params[:supplier_id]
      #   render json: {message: 'Error(s) occurred', errors: "Supplier not found"}, status: 422 unless current_user
      # end

      def bid_params
        params.require(:bid).permit(:amount,:start_date,:message)
      end
    end
  end
end
