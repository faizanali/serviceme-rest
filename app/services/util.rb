class Util
  cattr_accessor :user_agent

  class << self

    def random
      rand(10000 .. 99999).to_s.last(4)
    end
    def prepend_zero()

    end

    def browser
      Browser.new(user_agent)
    end

  end


end