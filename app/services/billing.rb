class Billing

  attr_accessor :errors

  def initialize client, payment_params
    @client = client
    @payment_params = payment_params
  end

  def process_payment
    begin
    customer = find_or_create_customer
    create_card customer
    make_payment
    rescue Start::BankingError => e
      # Since it's a decline, Start::BankingError will be caught
      puts "Status is: #{e.http_status}"
      puts "Code is: #{e.code}"
      puts "Message is: #{e.message}"

    rescue Start::RequestError => e
      # Invalid parameters were supplied to Start's API

    rescue Start::AuthenticationError => e
      # There's something wrong with that API key you passed

    rescue Start::ProcessingError => e
      # There's something wrong on Start's end

    rescue Start::StartError => e
      # Display a very generic error to the user, and maybe send
      # yourself an email

    rescue => e
      # Something else happened, completely unrelated to Start
    end
  end


  protected

  def find_or_create_customer
    if @client.customer_id.blank?
      customer = Start::Customer.create(
          :name => @client.name,
          :email => @client.email
      )
      @client.update(customer_id: customer.id)
    else
      customer = Start::Customer.get(@client.customer_id)
    end
    customer
  end

  def create_card customer
    customer.cards.create(
        :card => {
            :name => "Abdullah Ahmed",
            :number => "4242424242424242",
            :exp_month => 11,
            :exp_year => 2016,
            :cvc => 123
        }
    )
  end

  def make_payment
    Start::Charge.create(
        :amount => @payment_params[:amount],
        :currency => "aed",
        :customer_id => @client.customer_id,
        :description => "Two widgets (test@example.com)"
    )
  end
end