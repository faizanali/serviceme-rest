class OfferDetail


  def initialize category_id
    @category = Category.find_by  id: category_id
  end


  def params
    @@params[@category.slug] if @category
  end



  private

  @@params = {
      "house-cleaning" => [:house_type, :size, :bedrooms, :bathrooms,:dusting, :trash_removal, :kitchen_counter, :bathroom_vanity, :window_washing]
  }
end