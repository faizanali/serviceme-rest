require 'active_support/concern'

module UserValidations
  extend ActiveSupport::Concern
  included do
    validates_presence_of :email, :message => "Email Address is Required."
    validates_presence_of :mobile, :message => "Mobile number is Required."
    validates_uniqueness_of :email, :message => "Email Address Already In Use. Have You Forgot Your Password?"
    validates_format_of :email, :with => /\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, :message => "Please Enter a Valid Email Address."
    validates_length_of :password, :minimum => 8, :message => "Password Must Be Longer Than 8 Characters.",  :on => :create
    validates_confirmation_of :password, :message => "Password Confirmation Must Match Given Password."
  end
end
