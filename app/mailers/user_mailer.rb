class UserMailer < ApplicationMailer


  def verification_email user
    @user = user
    mail to: user.email, from: "info@serviceme.org"
  end
end
