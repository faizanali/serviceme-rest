class Image

  include Mongoid::Document


  field :file

  mount_uploader :file, ImageUploader

  embedded_in :supplier

end