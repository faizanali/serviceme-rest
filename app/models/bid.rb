class Bid
  include Mongoid::Document
  included Mongoid::Timestamps


  belongs_to :supplier
  belongs_to :offer

  field :amount, type: Float
  field :start_date, type: Date
  field :message, type: String

end