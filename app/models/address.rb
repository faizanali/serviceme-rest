class Address
  include Mongoid::Document



  field :street_address
  field :city
  field :area
  field :landmark
  field :country

  embedded_in :supplier

end