class Offer
  include Mongoid::Document
  include Mongoid::Timestamps


  field :location, type: String
  field :currency, type: String
  field :bid_type, type: String
  field :done_in, type: Integer
  field :date_time, type: DateTime
  field :additional_req, type: String
  field :has_budget, type: Boolean
  field :price, type: Float
  field :detail, type: Hash
  has_many :bids
  belongs_to :client
  belongs_to :category



  def serializable_hash(options)
    h = super(options)
    h[:category_slug] = self.category.try(:slug)
    h[:category] = self.category.try(:name)
    h[:bids_count] = bids.count
    h
  end

end