class Client < User

  has_many :offers
  has_and_belongs_to_many :favourites, class_name: 'Supplier',inverse_of: :favourite_bys

  field :country
  field :city

  def signing_up_with_existing_number?
    errors.messages[:mobile].try(:first) == "Mobile number already in use" && from_mobile && step == 1
  end

  def signing_up_unverified

  end
end