class Device
  include Mongoid::Document
  include Mongoid::Timestamps

  field :token,                        :type => String, default: ''
  field :push_notification_id,         :type => String, default: ''
  field :user_agent,                   :type => String, default: ''

  belongs_to :user
end