class Category
  include Mongoid::Document
  include Mongoid::Slug

  field :name
  field :description
  field :parent_id, type: BSON::ObjectId, default: 0
  slug :name
  has_and_belongs_to_many :suppliers
  has_many :offers


  validates_uniqueness_of :name

  scope :main, -> {where(parent_id: 0)}
  scope :sub, -> {where(:parent_id.ne => 0)}
  scope :trending, -> {sub.desc('_id').limit(4)}


  searchkick match: :word_start, searchable: [:name, :description]




  def subcategories
    Category.where(parent_id: self.id)
  end

  def serializable_hash(options = nil)
    h = super(options)
    h[:slug] = slug
    h[:subcategories] = subcategories
    h.delete('_slugs')
    h.delete('parent_id')
    h.delete(:subcategories)  unless self.parent_id.to_s == "0"
    h
  end


end
