class Supplier < User
  include Mongoid::Document
  included Mongoid::Timestamps

  field :company_name
  field :is_company, type: Boolean
  field :status, type: String, default: :online
  field :trade_license
  field :account_name
  field :bank_name
  field :account_no
  field :swift_code

  embeds_one :address

  embeds_many :images, cascade_callbacks: true
  mount_uploader :trade_license,ImageUploader

  validates_presence_of :company_name, if: Proc.new {|supplier| supplier.is_company}
  validates_presence_of :first_name, unless: Proc.new {|supplier| supplier.is_company}
  validates_presence_of :last_name, unless: Proc.new {|supplier| supplier.is_company}

  accepts_nested_attributes_for :images
  accepts_nested_attributes_for :address
  has_many :bids

  has_and_belongs_to_many :categories

  has_and_belongs_to_many :favourite_bys, class_name: 'Client', inverse_of: :favourites

  scope :online, -> {where(status: :online)}

  searchkick match: :word_start, searchable: [:first_name,:last_name,:company_name]



  def name
    "#{first_name} #{last_name}"
  end


  def offers
    categories.map(&:offers).flatten
  end

  def self.filter(params,user)
    suppliers = all
    suppliers = user.favourites if user && params[:favourite_only] == "true"
    suppliers = suppliers.where(status: :online) if params[:online] == "true"
    suppliers
  end

  def services
    categories.map{|c| {id: c.id.to_s,name:c.name}}
  end

  def as_json(options)
    super(include: :images, methods: [:services])
  end



end