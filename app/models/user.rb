class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include BCrypt

  attr_accessor  :password, :password_confirmation, :skip_verification

  field :email,              :type => String
  field :first_name,               :type => String
  field :last_name,               :type => String
  field :mobile,             :type => String
  field :dob,                :type => Date
  field :verification_code,  :type => String
  field :active,             :type => Boolean, default: false
  field :password_hash,      :type => String
  field :email_verified,     :type => Boolean, default: false
  field :from_mobile,     :type => Boolean, default: false
  field :confirmation_token, :type => String
  field :step, :type => Integer
  field :image

  mount_uploader :image, ImageUploader


  with_options if: Proc.new {|user| user.step == 1} do
  validates_presence_of :mobile, :message => "Mobile number is Required."
  validates_uniqueness_of :mobile, :message => "Mobile number already in use"
end

  with_options if: Proc.new {|user| (!user.from_mobile) || (user.from_mobile && user.step == 2)} do
    validates_presence_of :email, :message => "Email Address is Required."
    validates_presence_of :mobile, :message => "Mobile number is Required."
    validates_presence_of :password, :message => "Password is Required.", unless: Proc.new {|user| user.password_hash.present?}
    validates_uniqueness_of :email, :message => "Email Address Already In Use. Have You Forgot Your Password?"
    validates_format_of :email, :with => /\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, :message => "Please Enter a Valid Email Address."
    validates_length_of :password, :minimum => 6, :message => "Password Must Be Longer Than 6 Characters.", unless: Proc.new {|user| user.password_hash.present?}
    validates_confirmation_of :password, :message => "Password Confirmation Must Match Given Password."
    validates_uniqueness_of :mobile, :message => "Mobile number already in use"
  end

  before_save :encrypt_password
  before_create :generate_verification_codes,:send_verification, :send_email, unless: Proc.new { |user| user.skip_verification }
  has_many :devices, dependent: :destroy

  def authenticate(password)
    return self.password_correct?(password)
  end

  def sign_in
    return nil unless active || skip_verification || !(step == 1)
    device = self.devices.create(token: SecureRandom.uuid,user_agent: Util.user_agent)
    device.token
  end

  def activate
    self.update(active: true, verification_code: nil)
  end

  def send_verification
    begin
      $twilio.messages.create(
          from: '+12562729038',
          to: self.mobile,
          body: "Welcome\n#{self.verification_code} is your ServiceME verification code."
      ) if self.mobile.present?
    rescue Twilio::REST::RequestError => e
      self.errors.add(:mobile, e.message)
      false
    end
  end

  def send_email
    UserMailer.delay.verification_email(self) if self.email.present?
  end

  def self.from_auth(auth_hash)
    auth_hash = auth_hash.with_indifferent_access
    user = find_or_create_by(email: auth_hash['email'])
    user.first_name = auth_hash['first_name']
    user.last_name =  auth_hash['last_name']
    user.email = auth_hash['email']
    user.password = Util.random
    user.email_verified = true
    user.skip_verification = true
    user.save(validate: false)
    user
  end

  def profile_completed?
    !!first_name && !!email && !!mobile && !!password_hash
  end

  def serializable_hash(options = nil)
    h = super(options)
    h[:profile_completed] = profile_completed?
    h[:type] = _type
    h
  end

  def change_password(params)
    if self.password_correct?(params[:current_password])
      self.update(password: params[:password])
    else
      self.errors.add(:password,"Invalid Password")
      false
    end
  end

  protected

  def password_correct?(password)
    user_pass = Password.new(self.password_hash)
    user_pass == password
  end


  def generate_verification_codes
    self.verification_code = Util.random
    self.confirmation_token = SecureRandom.uuid
  end



  def encrypt_password
    self.password_hash = Password.create(@password) if @password.present?
  end

end